# cc-helpers

## Description
Set of helpers to simplify work with Crypto Currency apis

## Packages
bsc: helpers for https://bscscan.com/apis

## Installation
Pip command: `pip install cc-helpers --index-url https://gitlab.com/api/v4/projects/16352053/packages/pypi/cc-helpers`

## Authors and acknowledgment
TBD

## License
TBD

## Project status
TBD
