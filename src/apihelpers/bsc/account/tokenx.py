import json
import urllib3
import time


class BEP20Tokens:
    '''
        https://docs.bscscan.com/api-endpoints/accounts#get-a-list-of-bep-20-token-transfer-events-by-address
    '''
    page = 1
    data = []

    def __init__(self, bsc_scan_api_key: str, bep_20_address: str):
        self.apikey = bsc_scan_api_key
        self.address = bep_20_address

    def __get_url(self, page: int = page):
        return '''https://api.bscscan.com/api
                    ?module=account
                    &action=tokentx
                    &address={address}
                    &apikey={api_key}
                    &page={page}
                    &offset=100
                    &startblock=0
                    &endblock=999999999
                    &sort=asc
                '''.replace('\n', '').replace('\r\n', '').replace(' ', '') \
            .format(
                address=self.address,
                api_key=self.apikey,
                page=page
            )

    def __loader(self, page: int = page):
        http = urllib3.PoolManager()

        resp = http.request('GET', self.__get_url(page))
        data = json.loads(resp.data)

        if int(data['status']) == 0:
            ''' write log '''
            return

        if page % 5 == 0:  # API Limit 5 RPS
            time.sleep(1)

        self.data.append(data['result'])
        self.__loader(page + 1)

    def all(self):
        self.__loader()

        return self.data
